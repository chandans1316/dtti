(function($){

  //Scorecard Ordering
  $(".admin-scorecard-body").sortable();

  $(".save-scorecard-order").prop("disabled", true);

  $(".admin-scorecard-body").on('sortupdate', function(){
    saveOrderClick();
  });

  var order;
  function saveOrderClick() {

    var trs = $(".admin-scorecard-body tr");
    order = [trs.size()];
    var index = 0;

    trs.each(function(index){
      if ($(this).data('id') !== undefined) {
        order.push($(this).data('id') + '=>' + index);
      }
      index++;
    });

    updateFormSubmission();

  }

  function updateFormSubmission(){
    var orderValue = $("#scorecard-order");
    var submit = $(".save-scorecard-order");

    orderValue.val(order);
    submit.prop("disabled", false);
  }

  //FAQ Ordering
  $(".admin-faq-body").sortable();

  $(".save-faq-order").prop("disabled", true);

  $(".admin-faq-body").on('sortupdate', function(){
    saveFaqOrderClick();
  });

  var faqOrder;
  function saveFaqOrderClick() {

    var trs = $(".admin-faq-body tr");
    faqOrder = [trs.size()];
    var index = 0;

    trs.each(function(index){
      if ($(this).data('id') !== undefined) {
        faqOrder.push($(this).data('id') + '=>' + index);
      }
      index++;
    });

    updateFaqFormSubmission();

  }

  function updateFaqFormSubmission(){
    var orderValue = $("#faq-order");
    var submit = $(".save-faq-order");

    orderValue.val(faqOrder);
    submit.prop("disabled", false);
  }

  // Warn Before Deletion
  $(".delete-entry").click(function(evt){
    evt.preventDefault();

    var $that = $(this);

    var redirectUrl = $that.data('url');
    var cardValue = $that.data('cardvalue');
    var type = $that.data('type');

    cardValue = cardValue.replace(/(<\/?[^>*]>)/g, '');

    var redirect = window.confirm("Are you sure want to delete the following "  + type + "?" + ' ' + cardValue);

    if (redirect === true) {
      window.location.href = redirectUrl;
    }

  });

  //Other Admin Functions
  $("#datepicker").datepicker();

  $('#sort-me').tablesorter();

  $('#sort-me th').click(function() {
    return false;
  });

  $('.btn-export').click(function () {
    $(this).find('.ficon-save').addClass('ficon-spin');
  });

  Shadowbox.init();

})(jQuery);
