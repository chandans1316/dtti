$(function() {
	$("#datepicker").datepicker();

	$('#sort-me').tablesorter();

	$('#sort-me th').click(function() {
		return false;
	});

	$('.btn-export').click(function () {
		$(this).find('.ficon-save').addClass('ficon-spin');
	});
});

Shadowbox.init();