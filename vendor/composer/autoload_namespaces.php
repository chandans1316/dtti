<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Whoops' => array($vendorDir . '/filp/whoops/src'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Symfony\\Component\\Routing\\' => array($vendorDir . '/symfony/routing'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\HttpKernel\\' => array($vendorDir . '/symfony/http-kernel'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Symfony\\Component\\DomCrawler\\' => array($vendorDir . '/symfony/dom-crawler'),
    'Symfony\\Component\\Debug\\' => array($vendorDir . '/symfony/debug'),
    'Symfony\\Component\\CssSelector\\' => array($vendorDir . '/symfony/css-selector'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Symfony\\Component\\BrowserKit\\' => array($vendorDir . '/symfony/browser-kit'),
    'Robbo\\Hashing' => array($vendorDir . '/robclancy/laravel4-hashing/src'),
    'Predis' => array($vendorDir . '/predis/predis/lib'),
    'Patchwork' => array($vendorDir . '/patchwork/utf8/class'),
    'PasswordLib' => array($vendorDir . '/passwordlib/passwordlib/lib'),
    'PHPParser' => array($vendorDir . '/nikic/php-parser/lib'),
    'Normalizer' => array($vendorDir . '/patchwork/utf8/class'),
    'Mailgun\\Tests' => array($vendorDir . '/mailgun/mailgun-php/tests'),
    'Mailgun' => array($vendorDir . '/mailgun/mailgun-php/src'),
    'Keboola\\Csv' => array($vendorDir . '/keboola/csv/src'),
    'Jeremeamia\\SuperClosure' => array($vendorDir . '/jeremeamia/SuperClosure/src'),
    'Illuminate' => array($vendorDir . '/laravel/framework/src'),
    'Guzzle\\Tests' => array($vendorDir . '/guzzle/guzzle/tests'),
    'Guzzle' => array($vendorDir . '/guzzle/guzzle/src'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib'),
    'ClassPreloader' => array($vendorDir . '/classpreloader/classpreloader/src'),
    'Carbon' => array($vendorDir . '/nesbot/carbon/src'),
    'Buzz' => array($vendorDir . '/kriswallsmith/buzz/lib'),
    'Bogardo\\Mailgun\\' => array($vendorDir . '/bogardo/mailgun/src'),
    '' => array($vendorDir . '/scottrobertson/premailer/src', $vendorDir . '/scottrobertson/premailer/tests'),
);
