<?php

class NewsController extends BaseController
{

	public function showNews($slug)
	{
		// Try to load subpage from slug
		$postTypeSubpage = PostTypeSubpage::pageFromSlug($slug);

		// If page was not found, invoke 404
		if (!$postTypeSubpage) {
			App::abort(404, 'Page not found');
		}

		// Retreive the post from the subpage object
		$post = $postTypeSubpage->post;

		// Show the view
		return View::make('pages.news')
			->with('slug', $slug)
			->with('post', $post);

	}

}