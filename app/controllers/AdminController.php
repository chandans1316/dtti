<?php

class AdminController extends BaseController {
	
	public function __construct()
	{
		$this->beforeFilter('auth');
	}

	public function getIndex()
	{
		$posts = Post::orderBy('order')->get();

		foreach ($posts as $post) :
			if ($post->post_type == 'SUBPAGE' || $post->post_type == 'ARTICLE')
			{
				if (isset($post->postType->source) && !empty($post->postType->source))
				{
					$url = parse_url($post->postType->source);
					$post->postType->host = $url['host'];
				}
			}

			if ($post->post_type == 'VIDEO')
			{
				if (!isset($post->postType->image_file_name) || empty($post->postType->image_file_name))
				{
					$post->postType->thumbnail = 'http://img.youtube.com/vi/'.$post->postType->youtube_id.'/hqdefault.jpg';
				}
				else
				{
					$post->postType->thumbnail = 'uploads/'.$post->postType->image_file_name;
				}
			}

		endforeach;

		return View::make('pages.admin.dashboard')
			->with('posts', $posts);
	}

	public function getStates() {

		$states = State::take(1000)->orderBy('name','ASC')->get();

		echo View::make('pages.admin.statelist')->with('states',$states);

	}
}