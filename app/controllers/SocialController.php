<?php

class SocialController extends BaseController {

  public function getIndex()
  {
    $current_page = 'social';

    $nf = new Newsfeed();

    $social_initial = $nf->getItems();
    $social = $social_initial['facebook'] + $social_initial['twitter'];
    usort($social, function($a, $b){
      if ($a->timestamp === $b->timestamp) {
        return 0;
      }
      return ($a->timestamp > $b->timestamp) ? -1 : 1;
    });

    $social = array_slice($social, 0, 25);

    return View::make('pages.social')
                 ->with('current_page', $current_page)
                 ->with('social', $social);
  }

}