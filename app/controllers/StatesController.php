<?php

class StatesController extends BaseController
{

    public function showState() {
        $states = State::where('url', '=', Request::segment(1))
            ->take(1)
            ->get();

        if (count($states) < 1) {
            App::abort(404);
        }

        $list = State::where('dropdown', '=', 1)
            ->take(1000)
            ->orderBy('name', 'ASC')
            ->get();

        $state = $states[0];

        return View::make('pages.indstate')
            ->with('state', $state)
            ->with('list', $list);
    }

}