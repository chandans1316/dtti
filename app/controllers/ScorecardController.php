<?php

class ScorecardController extends BaseController {

	public function getIndex()
	{
		$current_page = 'scorecard';

    $officials = DB::table('scorecard')
                      ->where('id', '>=', 1)
                      ->orderBy('order', 'asc')
                      ->get();


    foreach ($officials as $official) {
      $official->twitter = urlencode('@' . $official->twitter . ' Don\'t support a burdensome tax on the American people. Please vote #NoNetTax! www.DontTaxTheInter.net');
    }

		return View::make('pages.scorecard')
      ->with('officials', $officials)
			->with('current_page', $current_page);
	}

}
