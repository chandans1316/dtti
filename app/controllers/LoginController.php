<?php

class LoginController extends BaseController {
	
	public function __construct()
	{
		//construct
	}

	public function getIndex()
	{
		if (Auth::check())
			return Redirect::to('admin');

		return View::make('pages.admin.login');
	}

	public function postIndex()
	{	
		$rules = array(
			'email' => array('required', 'max:255'),
			'password' => array('required', 'max:255')
		);

		$validator = Validator::make(Input::all(), $rules);
		
		if ($validator->fails())
		{
			return Redirect::to('/login')
				->withInput()
				->withErrors($validator);
		}
		else
		{
			$email = Input::get('email');
			$password = Input::get('password');

			if (Auth::attempt(array('email' => $email, 'password' => $password), TRUE))
			{
				return Redirect::to('admin');
			}
			else 
			{
				return Redirect::to('login')
					->withInput()
					->with('login_error', TRUE);
			}
		}
	}
}