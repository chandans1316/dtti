<?php

class TweetsController extends BaseController {

	public function getIndex()
	{
		// Cache tweets
		$tweets = Cache::remember('tweets', 5 /* 5 minutes */, function()
		{

			// Initialize Twitter
			$twitter = new Endroid\Twitter\Twitter(
				Config::get('twitter.consumer_key'),
				Config::get('twitter.consumer_secret'),
				Config::get('twitter.access_token'),
				Config::get('twitter.access_token_secret')
			);

			// TODO: Change me!
			// @NoInternetTax dodes not currently have any favorited tweets, so we use a test
			// account for now. Change this to true for production use.
			$useNoTaxNetAccount = false;

			// Define parameters
			$parameters = $useNoTaxNetAccount ? array() : array('screen_name' => 'NoInternetTax');

			// Run query against the Twitter API
			$response = $twitter->query('favorites/list', 'GET', 'json', $parameters);

			return json_decode($response->getContent());
		});

		// Reset cache in case of errors
		if (isset($tweets->errors)) {
			Cache::forget('tweets');
		}

		return Response::json($tweets);
	}
}