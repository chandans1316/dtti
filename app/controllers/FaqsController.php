<?php

class FaqsController extends BaseController {

  public function getIndex()
  {
    $current_page = 'faqs';
    $body_class = 'subpage';

    $faqs = DB::table('faq')
                ->orderBy('order', 'asc')
                ->get();

    return View::make('pages.faqs')
      ->with('faqs', $faqs)
      ->with('current_page', $current_page)
      ->with('body_class', $body_class);
  }

}