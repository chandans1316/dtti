<?php

class UpdateController extends BaseController
{

	public function __construct()
	{
		$this->beforeFilter('auth');
	}

	function updatePost($id = NULL)
	{
		$post = Post::find($id);

		if (!$post)
		{
			Session::flash('message', 'Could not update. Post does not exist!');
			return Redirect::to('admin');
		}

		if (Request::getMethod() == 'POST')
		{
			if ($post->post_type == 'QUOTE')
			{
				$rules = array(
					'author' => array('required', 'max:255'),
					'text' => array('required', 'max:2000'),
					'order' => array('required', 'integer')
				);

				$validator = Validator::make(Input::all(), $rules);

				if ($validator->fails())
				{
					return Redirect::to('update/' . $post->id)
						->withInput()
						->withErrors($validator);
				}

				$post->order = Input::get('order');

				$post->postType->author = Input::get('author');
				$post->postType->text = Input::get('text');
				$post->postType->save();
				$post->save();

				Session::flash('message', 'Quote was updated!');
				return Redirect::to('/update/' . $post->id);
			}
			elseif ($post->post_type == 'ARTICLE')
			{
				$rules = array(
					'title' => array('required', 'max:255'),
					'excerpt' => array('required', 'max:2000'),
					'image' => array('image', 'mimes:jpeg,png,jpg'),
					'source' => array('required', 'max:255','url'),
					'url' => array('required', 'max:255','url'),
					'order' => array('required', 'integer')
				);

				$validator = Validator::make(Input::all(), $rules);

				if ($validator->fails())
				{
					return Redirect::to('update/' . $post->id)
						->withInput()
						->withErrors($validator);
				}

				$post->order = Input::get('order');

				$post->postType->post_id = $post->id;
				$post->postType->title = Input::get('title');
				$post->postType->excerpt = Input::get('excerpt');
				$post->postType->url = Input::get('url');
				$post->postType->source = Input::get('source');

				if (Input::hasFile('image'))
				{
					$file = Input::file('image');
					$mime_type = $file->getMimeType();

					$ext = 'none';
					if ($mime_type == 'image/png')
						$ext = 'png';

					if ($mime_type == 'image/jpeg' || $mime_type == 'image/pjpeg')
						$ext = 'jpg';

					$file_name = md5(Str::random(20).time()).'.'.$ext;
					$file->move('uploads', $file_name);

					$post->postType->image_file_name = $file_name;
				}

				$post->save();
				$post->postType->save();

				Session::flash('message', 'Article was updated!');
				return Redirect::to('/update/' . $post->id);
			}
			elseif ($post->post_type == 'SUBPAPGE')
			{
				$rules = array(
					'title' => array('required', 'max:255'),
					'source' => array('required', 'max:255', 'url'),
					'author' => array('required', 'max:255'),
					'date' => array('required', 'max:255'),
					'excerpt' => array('required', 'max:2000'),
					'content' => array('required', 'max:2000'),
					'thumb_image' => array('image', 'mimes:jpeg,png,jpg'),
					'banner_image' => array('image', 'mimes:jpeg,png,jpg'),
					'order' => array('required', 'integer')
				);

				$validator = Validator::make(Input::all(), $rules);

				if ($validator->fails())
				{
					return Redirect::to('update/' . $post->id)
						->withInput()
						->withErrors($validator);
				}

				$post->order = Input::get('order');

				$post->postType->post_id = $post->id;
				$post->postType->title = Input::get('title');
				$post->postType->source = Input::get('source');
				$post->postType->date = Input::get('date');
				$post->postType->excerpt = Input::get('excerpt');
				$post->postType->content = Input::get('content');

				if (Input::hasFile('thumb_image'))
				{
					$file = Input::file('thumb_image');
					$mime_type = $file->getMimeType();

					$ext = 'none';
					if ($mime_type == 'image/png')
						$ext = 'png';

					if ($mime_type == 'image/jpeg' || $mime_type == 'image/pjpeg')
						$ext = 'jpg';

					$file_name = md5(Str::random(20).time()).'.'.$ext;
					$file->move('uploads', $file_name);

					$post->postType->thumb_file_name = $file_name;
				}

				if (Input::hasFile('banner_image'))
				{
					$file = Input::file('banner_image');
					$mime_type = $file->getMimeType();

					$ext = 'none';
					if ($mime_type == 'image/png')
						$ext = 'png';

					if ($mime_type == 'image/jpeg' || $mime_type == 'image/pjpeg')
						$ext = 'jpg';

					$file_name = md5(Str::random(20).time()).'.'.$ext;
					$file->move('uploads', $file_name);

					$post->postType->thumb_file_name = $file_name;
				}

				$post->save();
				$post->postType->save();

				Session::flash('message', 'Subpage was updated!');
				return Redirect::to('/update/' . $post->id);
			}
			elseif ($post->post_type == 'VIDEO') {
				$rules = array(
					'title' => array('required', 'max:255'),
					'image' => array('image', 'mimes:jpeg,png,jpg'),
					'description' => array('max:1000'),
					'youtube_url' => array('required', 'url', 'max:1000'),
					'order' => array('required', 'integer')
				);

				$validator = Validator::make(Input::all(), $rules);

				if ($validator->fails())
				{
					return Redirect::to('update/'.$post->id)
						->withInput()
						->withErrors($validator);
				}

				$url = Input::get('youtube_url');
				$parsed_url = parse_url($url);
				parse_str(parse_url($url, PHP_URL_QUERY ), $url_pieces);

				if (!preg_match('#youtube#', $parsed_url['host']) || !isset($url_pieces['v']) || empty($url_pieces['v']))
				{
					return Redirect::to('/update/' . $post->id)
						->withInput()
						->with('message', 'That YouTube URL is not valid!');
				}

				$post->order = Input::get('order');

				$post->postType->post_id = $post->id;
				$post->postType->title = Input::get('title');
				$post->postType->description = Input::get('description');
				$post->postType->youtube_id = $url_pieces['v'];

				if (Input::hasFile('image'))
				{
					$file = Input::file('image');
					$mime_type = $file->getMimeType();

					$ext = 'none';
					if ($mime_type == 'image/png')
						$ext = 'png';

					if ($mime_type == 'image/jpeg' || $mime_type == 'image/pjpeg')
						$ext = 'jpg';

					$file_name = md5(Str::random(20).time()).'.'.$ext;
					$file->move('uploads', $file_name);

					$post->postType->image_file_name = $file_name;
				}

				$post->save();
				$post->postType->save();

				Session::flash('message', 'Video was updated!');
				return Redirect::to('/update/' . $post->id);
			}
		}
		else
		{
			return View::make('pages.admin.update.' . strtolower($post->post_type))
				->with('post', $post);
		}
	}

	function updateState($id = NULL)
	{
		$state = State::find($id);
		if (!$state)
		{
			Session::flash('message', 'Could not update. State does not exist!');
			return Redirect::to('admin');
		}

		if (Request::getMethod() == 'POST') {

			if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) &&
			     empty($_FILES) && (int) $_SERVER['CONTENT_LENGTH'] > 0)
			{
			  $displayMaxSize = ini_get('post_max_size');

			  switch(substr($displayMaxSize,-1))
			  {
			    case 'G':
			      $displayMaxSize = $displayMaxSize * 1024;
			    case 'M':
			      $displayMaxSize = $displayMaxSize * 1024;
			    case 'K':
			       $displayMaxSize = $displayMaxSize * 1024;
			  }

			  $error = 'Posted data is too large. '.
			           $_SERVER['CONTENT_LENGTH'].
			           ' bytes exceeds the maximum size of '.
			           $displayMaxSize.' bytes.  Please upload smaller files.';
			           return Redirect::to('updatestate/'.$state->id)
						->withInput()->with('message',$error);
			}

			$rules = array(
				'name' => array('required', 'max:255'),
				'headline' => array('required', 'max:255'),
				'subheadline' => array('required', 'max:255'),
				'url' => array('required', 'max:2000'),
				'image_file_name' => array('between:0,1024','image', 'mimes:jpeg,png,jpg'),
				'pdf_file_name' => array('between:0,1024','mimes:pdf'),
				'body' => array('required', 'max:30000'),
			);

			$validator = Validator::make(Input::all(), $rules);

			if ($validator->fails())
			{

				return Redirect::to('updatestate/'.$state->id)
					->withInput()
					->withErrors($validator);
			}


			$state->name = Input::get('name');
			$state->headline = Input::get('headline');
			$state->subheadline = Input::get('subheadline');
			$state->url = Input::get('url');
			$state->body = Input::get('body');
			$state->dropdown = Input::get('dropdown')=='on' ? true : false;

			if (Input::hasFile('image_file_name'))
			{
				$file = Input::file('image_file_name');
				$mime_type = $file->getMimeType();

				$ext = 'none';
				if ($mime_type == 'image/png')
					$ext = 'png';

				if ($mime_type == 'image/jpeg' || $mime_type == 'image/pjpeg')
					$ext = 'jpg';

				$file_name = md5(Str::random(20).time()).'.'.$ext;
				$file->move('uploads', $file_name);

				$state->image_file_name = $file_name;
			}

			if (Input::hasFile('pdf_file_name'))
			{
				$file = Input::file('pdf_file_name');
				$mime_type = $file->getMimeType();

				$ext = 'none';
				if ($mime_type == 'application/pdf' || $mime_type == 'application/x-pdf')
					$ext = 'pdf';


				$file_name = md5(Str::random(20).time()).'.'.$ext;
				$file->move('uploads', $file_name);

				$state->pdf_file_name = $file_name;
			}

			$state->save();

			Session::flash('message', 'State successfully saved!');
			return Redirect::to('updatestate/'.$state->id);
		}
		else
		{

			return View::make('pages.admin.update.state')
				->with('state', $state);
		}


	}

	/**
	* Handles submission of update form
	**/
	function postScorecard( $id = NULL ) {

		$id = intval($id);

		if ( ($id === NULL) || (!is_int($id)) ) {

			Session::flash('message', 'Unable to find that entry. Please try again!');
			return Redirect::to('createscorecard');

		} else {

			/**
			* $post = array(
				'_token' => form token,
				'picture' => file,
				'first_name' => string,
				'last_name' => string,
				'party' => string,
				'supports_tax' => bool
				'twitter' => string,
				'facebook' => string
			);
			**/
			$post = Input::all();

			/**
			* Process Post values
			**/
			$scorecard = Scorecard::find($id);

			if ( !empty($post['supports_tax'])) {

				$scorecard->supports_tax = $post['supports_tax'];

			}

			if ( !empty($post['first_name']) && is_string($post['first_name']) ){
				$scorecard->first_name = $post['first_name'];
			}

			if ( !empty($post['last_name']) && is_string($post['last_name']) ) {
				$scorecard->last_name = $post['last_name'];
			}

			if ( !empty($post['party']) && is_string($post['party']) ) {
				$scorecard->party = $post['party'];
			}

			if ( !empty($post['twitter']) && is_string($post['twitter']) ) {
				$scorecard->twitter = $post['twitter'];
			}

			if ( !empty($post['facebook']) && is_string($post['facebook']) ) {
				$scorecard->facebook = $post['facebook'];
			}

			if ( !empty($post['picture']) && $post['picture']->isValid() ) {

				$file_extension = '.' . $post['picture']->getClientOriginalExtension();

				$img_dir = public_path() . '/img' . '/' . 'scorecards';
				$file_name = ( (!empty($post['first_name'])) ? $post['first_name'] : $scorecard->first_name ) . ( (!empty($post['last_name'])) ? $post['last_name'] : $scorecard->last_name ) . time() . $file_extension;
				$file_name = preg_replace('/\s/', '_', $file_name);

				$post['picture']->move($img_dir, $file_name);

				$scorecard->picture = URL::to('/') . '/img' . '/scorecards' . '/' . $file_name;

			}

			$scorecard->save();

			Session::flash('message', 'Updated scorecard for ' . $scorecard->first_name . ' ' . $scorecard->last_name . '!');
			return Redirect::to('create/scorecard');

		}



		//Add in to handle updating form.

		//Session::flash('message', 'Updated ' .  . 'to scorecard!');
		return Redirect::to('createscorecard');
	}

	/**
	* Returns with prepopulated scorecard form.
	**/
	function getScorecard( $id = NULL ) {
		$id = intval($id);

		if ( ($id === NULL) || (!is_int($id)) ) {

			Session::flash('message', 'Unable to find that entry. Please try again!');
			return Redirect::to('create/scorecard');

		} else {

			$scorecard_to_edit = Scorecard::find($id);

			$scorecards_initial = DB::table('scorecard')
                          ->where('id', '>=', 1)
                          ->orderBy('order', 'asc')
                          ->get();
			$scorecards = array();

			foreach ($scorecards_initial as $scorecard){
				$scorecards[] = array(
					'id' => $scorecard->id,
					'order' => $scorecard->order,
					'first_name' => $scorecard->first_name,
					'last_name' => $scorecard->last_name,
					'party' => $scorecard->party,
					'supports_tax' => $scorecard->supports_tax,
					'twitter' => $scorecard->twitter,
					'facebook' => $scorecard->facebook,
					'picture_url' => $scorecard->picture
					);
			}

		}

		return View::make('pages.admin.update.scorecard')
						->with('scorecard_to_edit', $scorecard_to_edit)
						->with('scorecards', $scorecards);

	}

	/**
	* Updates Order of Scorecards
	**/
	public function updateScorecardOrder()
	{
		$order = Input::get('scorecard_order');
		if (!empty($order)) {

			$order = explode(',', $order);

			foreach ($order as $id) {

				if ( !empty($id) && (strpos($id, '=>') != false) ){
					$vals = explode('=>', $id);

					$scorecard = Scorecard::find(intval($vals[0]));
					$scorecard->order = intval($vals[1]);
					$scorecard->save();
				}
			}

		} else {

			Session::flash('message', 'Unable to save reordered scorecards!');

		}

		return Redirect::to('create/scorecard');
	}

	/**
	* Returns FAQ to edit
	**/
	public function getFaq($id = null) {

		$id = intval($id);

		if ( ($id === NULL) || (!is_int($id)) ) {

			Session::flash('message', 'Unable to find that entry. Please try again!');
			return Redirect::to('create/faq');

		} else {

			$faq_to_edit = FAQ::find($id);

			$isFAQ = true;

			$faqs_initial = DB::table('faq')
													->where('id', '>=', 1)
													->orderBy('order', 'asc')
													->get();
			$faqs = array();

			foreach ($faqs_initial as $faq) {
				$f['id'] = $faq->id;
				$f['order'] = $faq->order;
				$f['question'] = $faq->question;
				$f['answer'] = $faq->answer;

				array_push($faqs, $f);
			}

		return View::make('pages.admin.update.FAQ')
								 ->with('isFAQ', $isFAQ)
								 ->with('faqs', $faqs)
								 ->with('faq_to_edit', $faq_to_edit);
		}
	}

	/**
	* Handles submission of post to update
	**/
	public function postFaq($id = null) {

		$id = intval($id);

		if ( ($id === NULL) || (!is_int($id)) ) {

			Session::flash('message', 'Unable to find that entry. Please try again!');
			return Redirect::to('create/faq');

		} else {

			/**
			* $post = array(
				'_token' => form token,
				'question' => string of html
				'answer' => string of html
			);
			**/
			$post = Input::all();

			/**
			* Process Post values
			**/
			$faq = FAQ::find($id);

			if (!empty($post['question'])) {
				$faq->question = $post['question'];
			} else {
				Session::flash('message', '1Unable to edit the question: ' . $faq->question . 'Please ensure both question and answer are filled in.');
				return Redirect::route('updatefaq', array('id' => $faq->id));
			}

			if (!empty($post['answer'])) {
				$faq->answer = $post['answer'];
			} else {
				Session::flash('message', '2Unable to edit the question: ' . $faq->question . 'Please ensure both question and answer are filled in.');
				return Redirect::route('updatefaq', array('id' => $faq->id));
			}

			$faq->save();

			Session::flash('message', 'Updated FAQ for the following question:' . $faq->question);
			return Redirect::route('updatefaq', array($faq->id));
		}

	}

	/**
	* Updates Order of FAQs
	**/
	public function updateFaqOrder() {

		$order = Input::get('faq_order');
		if (!empty($order)) {

			$order = explode(',', $order);

			foreach ($order as $id) {

				if ( !empty($id) && (strpos($id, '=>') != false) ){
					$vals = explode('=>', $id);

					$faq = FAQ::find(intval($vals[0]));
					$faq->order = intval($vals[1]);
					$faq->save();
				}
			}

			Session::flash('message', 'FAQ order saved!');

		} else {

			Session::flash('message', 'Unable to save reordered FAQs!');

		}

		return Redirect::to('create/faq');

	}

}