<?php

class Post extends Eloquent {

	public function postType()
	{
		if ($this->post_type == 'ARTICLE')
		{
			return $this->hasOne('PostTypeArticle', 'post_id');
		}
		elseif ($this->post_type == 'QUOTE')
		{
			return $this->hasOne('PostTypeQuote', 'post_id');
		}
		elseif ($this->post_type == 'SUBPAGE')
		{
			return $this->hasOne('PostTypeSubpage', 'post_id');
		}
		elseif ($this->post_type == 'VIDEO')
		{
			return $this->hasOne('PostTypeVideo', 'post_id');
		}
		else
		{
			throw new \RuntimeException('Invalid post type: ' . $this->post_type);
		}
	}

	public function user()
	{
		return $this->belongsTo('User');
	}
	
}