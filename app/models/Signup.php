<?php

class Signup extends Eloquent {

	public function getNameAttribute($value)
    {
        return trim($this->attributes['first_name'] . ' ' . $this->attributes['last_name']);
    }
	
}