<?php

class PostTypeSubpage extends Eloquent {

	public $timestamps = FALSE;
	protected  $table = 'post_type_subpage';

	// A little helper function to easier get a subpage object from a slug
	public static function pageFromSlug($slug)
	{
		return PostTypeSubpage::where('slug', '=', $slug)->first();
	}

	public function post()
	{
		return $this->belongsTo('Post', 'post_id');
	}

}