@if (count($scorecards) >= 1)

<table class="admin-scorecards-table" id="admin-scorecards-table tablesorter">
  <thead class="admin-scorecard-head">
    <th>Thumbnail</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Party</th>
    <th>Supports Tax?</th>
    <th>Twitter</th>
    <th>Facebook</th>
    <th></th>
    <th></th>
  </thead>
  <tbody class="admin-scorecard-body">
  @foreach ($scorecards as $scorecard)
  <tr data-order="{{ $scorecard['order'] }}" data-id="{{ $scorecard['id'] }}">
    <td class="scorecard-cell @if (empty($scorecard['picture_url']))empty@endif "> @if (!empty($scorecard['picture_url']))<img class="scorecard-thumb" src="{{ $scorecard['picture_url'] }}">@endif </td>
    <td class="scorecard-cell @if (empty($scorecard['first_name']))empty@endif ">{{ $scorecard['first_name'] }}</td>
    <td class="scorecard-cell @if (empty($scorecard['last_name']))empty@endif ">{{ $scorecard['last_name'] }}</td>
    <td class="scorecard-cell @if (empty($scorecard['party']))empty@endif ">{{ $scorecard['party'] }}</td>
    <td class="scorecard-cell @if (($scorecard['supports_tax'] != 0 && $scorecard['supports_tax'] != 1))empty@endif ">
      @if ($scorecard['supports_tax'] == 'y')
        Yes
      @elseif ($scorecard['supports_tax'] == 'n')
        No
      @elseif ($scorecard['supports_tax'] == 'u')
        Unknown
      @endif
    </td>
    <td class="scorecard-cell @if (empty($scorecard['twitter']))empty@endif ">{{ $scorecard['twitter'] }}</td>
    <td class="scorecard-cell @if (empty($scorecard['facebook']))empty@endif ">{{ $scorecard['facebook'] }}</td>
    <td><a href="{{ URL::to('updatescorecard', array( $scorecard['id'] )); }}">Edit</a></td>
    <td><a class="delete-entry" href="{{ URL::to('deletescorecard', array( $scorecard['id'])); }}" data-url="{{ URL::to('deletescorecard', array( $scorecard['id'] )); }}" data-cardvalue="{{ $scorecard['first_name'] . ' ' . $scorecard['last_name'] }}" data-type="scorecard">Delete</a></td>
  </tr>
  @endforeach
  </tbody>
</table>

<form class="admin-scorecard-save" action="{{ URL::to('reorder/scorecards/'); }}" method="POST">
  <input id="scorecard-order" type="hidden" value="" name="scorecard_order">
  <input class="btn btn-primary save-scorecard-order" type="submit" value="Save">
</form>

@endif