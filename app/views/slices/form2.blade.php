@if (Session::has('success_message'))
        <div class="message success">
          {{ Session::get('success_message') }}
        </div>
        @else
        <div id="success-message" style="display: none;"></div>
        {{ Form::open(array('url' => '/', 'data-validate' => 'parsley', 'id' => 'signup-form')) }}

        {{ Form::hidden('source', Request::path()) }}

        <div class="first-name-wrap input-wrap">
          {{ Form::label('first_name', 'First Name', array('id' => 'first-name-label')); }}
          {{
            Form::text(
              'first_name',
              Input::old('first_name'),
              array(
                'placeholder' => 'First Name',
                'class' => 'firstname '.$errors->first('first_name', 'error'),
                'data-trigger' => 'change',
                'data-required' => 'true',
                'data-maxlength' => '255'
              )
            )
          }}
          {{ $errors->first('first_name', '<div class="error-message">:message</div>') }}
        </div>

        <div class="last-name-wrap input-wrap">
          {{ Form::label('last_name', 'Last Name', array('id' => 'last-name-label')); }}
          {{
            Form::text(
              'last_name',
              Input::old('last_name'),
              array(
                'placeholder' => 'Last Name',
                'class' => 'lastname '.$errors->first('last_name', 'error'),
                'data-trigger' => 'change',
                'data-required' => 'true',
                'data-maxlength' => '255',
              )
            )
          }}
          {{ $errors->first('last_name', '<div class="error-message">:message</div>') }}
        </div>

        <div class="email-wrap input-wrap">
          {{ Form::label('email', 'Email', array('id' => 'email-label')); }}
          {{
            Form::text(
              'email',
              Input::old('email'),
              array(
                'placeholder' => 'Email Address',
                'class' => 'email '.$errors->first('email', 'error'),
                'data-trigger' => 'change',
                'data-required' => 'true',
                'data-type' => 'email',
                'data-maxlength' => '255'
              )
            )
          }}
          {{ $errors->first('email', '<div class="error-message">:message</div>') }}
        </div>

        <div class="zip-wrap input-wrap">
          {{ Form::label('zip_code', 'Zip Code', array('id' => 'zip-code-label')); }}
          {{
            Form::text(
              'zip_code',
              Input::old('zip_code'),
              array(
                'placeholder' => 'Zip Code',
                'class' => 'zip '.$errors->first('zip_code', 'error'),
                'data-trigger' => 'change',
                'data-required' => 'true',
                'data-type' => 'digits',
                'data-maxlength' => '5'
              )
            )
          }}
          {{ $errors->first('zip_code', '<div class="error-message">:message</div>') }}
        </div>

        <div class="submit-wrap">
          {{ Form::submit('Submit', array('id' => 'submit-button')) }}
        </div><!--/submit wrap-->

        {{ Form::close() }}

        @endif



















