	<!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,700,600,300' rel='stylesheet' type='text/css'>-->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta
	<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<link rel="shortcut icon" type="image/png" href="/img/favicon.ico" /><!-- default favicon -->
	<title>@yield('title')</title>
	<meta name="description" content="For years, Americans have successfully fought attempts by big government advocates to aggressively tax and regulate the Internet. The &quot;Marketplace Fairness Act&quot; seeks to end that success by giving the federal government's blessing to an Internet tax collection scheme that would allow states to harass and audit businesses beyond their borders. Add your name below and tell your elected officials &quot;Don't tax the Internet!&quot;">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<!-- Styling for your grid blocks -->
	<link rel="stylesheet" href="/css/font-awesome.min.css">
	 <!--[if IE 7]><link rel="stylesheet" href="/css/font-awesome-ie7.min.css"><![endif]-->
	<link rel="stylesheet" href="/css/shadowbox.css">
	<link rel="stylesheet" href="/css/style.css">
	<script src="//cdn.optimizely.com/js/31243213.js"></script>
	<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion_async.js" charset="utf-8"></script>

	<script>
		var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-40222775-1']);
			_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>

    <script src="//platform.twitter.com/oct.js" type="text/javascript"></script>

	<script>(function() {
	  var _fbq = window._fbq || (window._fbq = []);
	  if (!_fbq.loaded) {
	    var fbds = document.createElement('script');
	    fbds.async = true;
	    fbds.src = '//connect.facebook.net/en_US/fbds.js';
	    var s = document.getElementsByTagName('script')[0];
	    s.parentNode.insertBefore(fbds, s);
	    _fbq.loaded = true;
	  }
	  _fbq.push(['addPixelId', '696179147099037']);
	})();
	window._fbq = window._fbq || [];
	window._fbq.push(['track', 'PixelInitialized', {}]);
	</script>
	<noscript><img height="1" width="1" border="0" alt="" style="display:none" src="https://www.facebook.com/tr?id=696179147099037&amp;ev=NoScript" /></noscript>

	<!-- Facebook OAauth -->
	<meta property="og:site_name" content="Don't Tax the Internet" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="http://donttaxtheinter.net/img/dtti.fb.png" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:title" content="Don't Tax the Internet" />
	<meta property="og:description" content="For years, Americans have successfully fought attempts by big government advocates to aggressively tax and regulate the Internet. The &quot;Marketplace Fairness Act&quot; seeks to end that success by giving the federal government's blessing to an Internet tax collection scheme that would allow states to harass and audit businesses beyond their borders. Add your name below and tell your elected officials &quot;Don't tax the Internet!&quot;" />
	<meta property="og:url" content="http://donttaxtheinter.net" />
