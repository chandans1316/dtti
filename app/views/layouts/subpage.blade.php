<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	@include('slices.head')
</head>

<body class="subpage">

<header>
<div class="inner">
<h2 id="logo">Tell Congress Don't Tax the Internet</h2>
<p>For years, Americans have successfully fought attempts by big government advocates to aggressively tax and regulate the Internet. The &quot;Marketplace Fairness Act&quot; seeks to end that success by giving the federal government's blessing to an Internet tax collection scheme that would allow states to harass and audit businesses beyond their borders. Add your name below and tell your elected officials &quot;Don't tax the Internet!&quot;</p>
 <ul class="social-links">
	 <li class="twitter-share">
	 <a href="https://twitter.com/intent/tweet?text=Protect Internet commerce and American small businesses. Tell Congress “Don’t tax the Internet!”&url=http://DontTaxtheInter.net&hashtags=NoNetTax" class="twitter">Tweet</a>
	 </li>
	 <li class="facebook-share">
	 <a class="facebook" onclick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=Protect+Internet+commerce+and+American+small+businesses.+Tell+Congress+%22Don%E2%80%99t+tax+the+Internet%21%22&amp;p[summary]=&amp;p[url]=http://DontTaxtheInter.net&amp;&amp;p[images][0]=http://engagedev.com/notaxnet/example/images/dtti-avatar-nolines.png','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)">Share</a>
	 </li>
 </ul>

</div>
	<section class="form-wrap">
	
		<div class="inner">
		<h2>Sign the Petition:</h2>
	<h4>Show your opposition to burdensome new taxes by adding your name below:</h4>

		@include('slices.form')
	
		<h5>Now... tell YOUR member of congress and senators <strong>&ldquo;Don&rsquo;t tax the Internet!&rdquo;</strong> <a href="http://taxeswithoutborders.com/ " target="_blank">Go Now&raquo;</a></h5>
		</div>
	</section>

</header>
  <div id="container">
	<div id="main" role="main">
		@yield('content')
	</div>
   </div>
   <footer>
   <a href="/privacy-policy" class="privacy">Privacy Policy</a>
	   <ul>
	   <li class="atr"><a href="http://www.atr.org/">http://www.atr.org/</a></li>
	   <li class="campaign"><a href="http://www.campaignforliberty.org/">http://www.campaignforliberty.org/</a></li>
	   <li class="cei"><a href="http://cei.org/">http://cei.org/</a></li>
	   <li class="freedom"><a href="http://freedomandprosperity.org/">http://freedomandprosperity.org/</a></li>
	   <li class="heartland"><a href="http://heartland.org/">http://heartland.org/</a></li>
	   <li class="ipi"><a href="http://www.ipi.org/">http://www.ipi.org/</a></li>
	   <li class="ntu"><a href="http://www.ntu.org/">http://www.ntu.org/</a></li>
	   <li class="rstreet"><a href="http://www.rstreet.org/">http://www.rstreet.org/</a></li>
	   </ul>
   </footer>
  <!-- include jQuery -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script>

  <!-- Include the plug-in -->
  <script src="//platform.twitter.com/widgets.js"></script>
  <script src="/js/jquery.parsley.js"></script>
  <script src="/js/jquery.twitter.js"></script>
  <script src="/js/scripts.js?v=2"></script>

  <!-- Once the page is loaded, initalize the plug-in. -->
  <script type="text/javascript">
	$(window).load(function() {
		
		// Asynchronously load webfont.js
		var wf = document.createElement('script');
		wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		wf.type = 'text/javascript';
		wf.async = 'true';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(wf, s);	
		
	});
  </script>
<div id="tweet-template">
	<div class="item">
		{PROFILEPIC} 
		<span class="name">{NAME}</span>
		<span class="user">@{HANDLE}</span>
		
		<div class="tweet-wrapper">
			<span class="text">{TEXT}</span>
			<span class="time">{AGO}</span>
			<a href="https://twitter.com/intent/tweet?in_reply_to={ID}" class="reply icon-reply-1">Reply</a>
			<a href="https://twitter.com/intent/retweet?tweet_id={ID}" class="retweet icon-retweet">Retweet</a>
			<a href="https://twitter.com/intent/favorite?tweet_id={ID}" class="favorite icon-star-1">Favorite</a>
        </div>
    </div>
</div>

</body>
</html>