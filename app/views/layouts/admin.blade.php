<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Admin | Don't Tax the Net</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Le styles -->
	<link href="/css/bootstrap.css" rel="stylesheet">
	<style type="text/css">
		body { padding-top: 60px; padding-bottom: 40px; }
	</style>
	<link href="/css/bootstrap-responsive.css" rel="stylesheet">
	<link href="/css/admin.css" rel="stylesheet">
	<link href="/css/admin.min.css" rel="stylesheet">
	<link href="/css/datepicker.css" rel="stylesheet">
	<link href="/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link href="/css/shadowbox.css" rel="stylesheet">

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link rel="shortcut icon" type="image/png" href="/img/favicon.png" /><!-- default favicon -->

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="//maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=visualization"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/bootstrap-datepicker.js"></script>
	<script src="/js/jquery.tablesorter.js"></script>
	<script src="/js/jquery-ui.min.js"></script>
	<script src="/js/jvector.js"></script>
	<script src="/js/usa.js"></script>
	<script src="/js/shadowbox.js"></script>

	@if ( !empty($isFAQ) )
		@if ( $isFAQ === true )
			<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
			<script>tinymce.init({ selector:'textarea',
														 plugins: "link",
	  												 menubar: "insert"
	  												 });</script>
		@endif
	@endif

	<link rel="stylesheet" href="/js/redactor/redactor.css" />
	<script src="/js/redactor/redactor.js"></script>


</head>
<body>
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="/admin">Don't Tax the Internet</a>
				<div class="nav-collapse pull-right">
					<ul class="nav">
						<li><a href="/"><i class="ficon-home"></i> Go Back to Home</a></li>
						@if (Auth::check())
							<li><a href="/logout"><i class="ficon-lock"></i> Logout</a></li>
						@endif
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>

	<div class="container">

		@if (Session::has('message'))
			<div class="alert notification">{{ Session::get('message') }}</div>
		@endif

		@section('back')

        @show

		@yield('content')

	</div>
	<script src="/js/admin.min.js"></script>
</body>
</html>