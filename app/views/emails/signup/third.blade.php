@extends('layouts.email')

@section('content')
	<p>{{{ $signup->first_name }}} &mdash;</p>

	<p>You’ve got friends. You use the Internet. So chances are you’ve got friends who also use the Internet.</p>

	<p>Don’t you think they ought to know about plans in Congress to put a tax on the Internet?</p>

	<p>Let your friends know about the Internet tax &mdash; and what they can do to stop it</p>
	
	<p style="text-align: center;">
		<a href="http://www.facebook.com/sharer.php?s=100&amp;p[title]=Protect+Internet+commerce+and+American+small+businesses.+Tell+Congress+%22Don%E2%80%99t+tax+the+Internet%21%22&amp;p[summary]=&amp;p[url]=http://DontTaxtheInter.net&amp;&amp;p[images][0]=http://engagedev.com/notaxnet/example/images/dtti-avatar-nolines.png"><img src="{{ url('img/share-facebook.png') }}" alt="Share on Facebook"></a>

		<a href="https://twitter.com/intent/tweet?text=Protect Internet commerce and American small businesses. Tell Congress “Don’t tax the Internet!”&amp;url=http://DontTaxtheInter.net&amp;hashtags=NoNetTax" class="twitter"><img src="{{ url('img/share-twitter.png') }}" alt="Share on Twitter"></a>
	</p>
@stop
