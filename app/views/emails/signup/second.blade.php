@extends('layouts.email')

@section('content')
	<p>{{{ $signup->first_name }}},</p>

	<p>Have your elected representatives in Congress heard from you about the Internet tax? We know they’ve heard from lobbyists for the supporters of the tax. Now they need to hear from you.</p>

	<p><strong><u><a href="http://www.ebaymainstreet.com/campaign/oppose-internet-sales-tax-lame-duck-legislation?utm_campaign=2014-internet-sales-tax&utm_source=partner-NoNetTax&utm_medium=referral">Click here to write a letter to your Senators and Representative and ask them to oppose a new tax on the Internet.</a></u></strong></p>

	<p>Research shows that hearing from constituents like you is one of the most important factors in a Member of Congress’ decision making progress. Make your voice heard today.</p>

	<p><strong>Don’t Tax the Internet<br>
	<a href="http://DontTaxTheInter.net">DontTaxTheInter.net</a></strong></p>
@stop