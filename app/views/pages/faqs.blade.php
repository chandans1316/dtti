@extends('layouts.base')

@section('title')
  FAQS | Don't Tax the Internet
@stop

@section('content')

  <main class="page-content">
    <div class="container">
      <h1 class="page-heading">Faqs</h1>

      @foreach ( $faqs as $key => $faq )

        <article class="faq">
          <h3>Q{{ $key + 1 }}: <?php echo(strip_tags($faq->question)); ?></h3>
          <button class="faq-toggle" aria-controls="faq-answer-{{ $key + 1 }}" aria-expanded="false" data-toggle="faq"><span class="fa fa-chevron-down"></span></button>
          <div id="faq-answer-{{ $key + 1 }}" class="faq-answer" role="region">
            {{ $faq->answer }}
          </div><!--/answer-->
        </article><!--/faq-->

      @endforeach

    </div><!--/container-->
  </main><!--/page content-->

@stop