@extends('layouts.base')

@section('title')
  Don't Tax the Internet
@stop

@section('content')

  <main class="page-content" data-masonry='{ "itemSelector": ".post-teaser", "columnWidth": ".post-teaser",  "gutter": 0, "percentPosition": true}'>

    <?php
      $i = 1;
      $background_image_index = 1;
      $background_color_index = 1;
    ?>

    @foreach ($posts as $post)

      <?php

        if ( $i % 2 != 0 ) {
          $background_class = 'image-bg';
          $bg_img = '/img/post-default-bg-' . $background_image_index . '.jpg';
          $background_image_index++;
        } else {
          $bg_img = '';

          if( $background_color_index % 2 != 0){
            $background_class = 'secondary-bg';
          }else{
            $background_class = 'primary-bg';
          }

          $background_color_index++;
        }

        if ( $background_image_index >= 5 ){
          $background_image_index = 1;
        }

      ?>

      <a class="article-link" href="{{ $post->postType->url }}" target="_blank">

      @if( $bg_img == '')
        <article class="post-teaser {{$background_class}}">
      @else
        <article class="post-teaser {{$background_class}}" style="background-image:url({{URL::to('/')}}{{$bg_img}})">
      @endif

        {{-- <time class="publication-date">1.15.2016</time> --}}
        <h3>{{ $post->postType->title }}</h3>
        <div class="website-link"><span>{{ $post->postType->host }}</span></div>
        <p class="excerpt">{{ $post->postType->excerpt }}</p>
      </article><!--/post teaser-->
      </a>
      <?php $i++; ?>

    @endforeach

  </main><!--/page content-->

      {{ $posts->links(); }}

@stop