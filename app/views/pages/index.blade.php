@extends('layouts.home')

@section('title')
	Don't Tax the Internet
@stop

@section('content')
	<ul id="tiles">
		@foreach ($posts as $post)

			@if ($post->post_type == 'QUOTE')
				<li class="quote"><h5><span class="icon-quote"></span>Quote</h5>
					<blockquote>{{ $post->postType->text }}</blockquote>
					<p>&mdash;{{ $post->postType->author }}</p>
				</li>
			@endif

			@if ($post->post_type == 'ARTICLE')
			<li class="news">
		        <h5><span class="icon-article-alt"></span>News</h5>

		        @if (!empty($post->postType->image_file_name))
					<img src="/uploads/{{ $post->postType->image_file_name }}" alt="Cato Institute Logo" />
		        @endif

		        <h3>{{ $post->postType->title }}</h3>
		        <p><a href="{{ $post->postType->source }}">{{ $post->postType->host }} //</a> {{ $post->postType->excerpt }}</p>
		        <a href="{{ $post->postType->url }}" class="read-more" target="_blank">Read&raquo;</a>
	        </li>
			@endif

	      @if ($post->post_type == 'SUBPAGE')
	       <li class="news">
	                <h5><span class="icon-article-alt"></span>News</h5>

	                @if (!empty($post->postType->thumb_file_name))
	              <img src="/uploads/{{ $post->postType->thumb_file_name }}">
	                @endif

	                <h3>{{ $post->postType->title }}</h3>
	                <p><a href="{{ $post->postType->source }}">{{ $post->postType->host }} //</a> {{ $post->postType->excerpt }}</p>
	                <a href="/news/{{ $post->postType->slug }}" class="read-more" target="_blank">Read&raquo;</a>
	              </li>
	        @endif

			@if ($post->post_type == 'VIDEO')
				<li class="video">
			        <h5><span class="awesome-font"><i class="ficon-facetime-video"></i></span>Video</h5>

			        
					<a rel="shadowbox" href="http://www.youtube.com/embed/{{ $post->postType->youtube_id }}?autoplay=1"><img src="{{ str_replace('http://','https://',$post->postType->thumbnail) }}"><div class="ficon-play"></div></a>
			        
			        <h3>{{ $post->postType->title }}</h3>

			        @if (!empty($post->postType->description))
			        	<p>{{ $post->postType->description }}</p>
			        @endif

		        </li>
			@endif
		@endforeach
        <!-- End of grid blocks -->
      </ul>

      <div class="pagination-container">
	      {{ $posts->links() }}
      </div>
@stop