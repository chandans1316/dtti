@extends('layouts.admin')

@section('back')
  <div class="row small-links">
    <div class="span12">
      <a href="/admin" class="btn btn-primary btn-large"><i class="ficon-arrow-left"></i> Go Back</a>
      <a href="/create/faq" class="btn btn-primary btn-large">Add New FAQ</a>
    </div>
  </div>
@stop

@section('content')

<div class="FAQ-create FAQ-form">

<h2 class="FAQ-form-header">Edit FAQ</h2>

{{ Form::model($faq_to_edit, array('route' => array('postupdatefaq', $faq_to_edit->id))) }}

<fieldset class="faq-question">
  <h2 class="FAQ-form-header">Question</h2>
  {{ Form::textarea('question', $faq_to_edit->question, ['placeholder' => 'Question']); }}
</fieldset>

<fieldset class="faq-question">
  <h2 class="FAQ-form-header">Answer</h2>
  {{ Form::textarea('answer', $faq_to_edit->answer, ['placeholder' => 'Answer']); }}
</fieldset>

  {{ Form::submit('Update FAQ'); }}

{{ Form::close() }}

</div>

@include('slices.faq', ['faqs' => $faqs]);

@stop
