@extends('layouts.admin')

@section('back')
  <div class="row small-links">
    <div class="span12">
      <a href="/admin" class="btn btn-primary btn-large"><i class="ficon-arrow-left"></i> Go Back</a>

      <a href="/create/scorecard" class="btn btn-primary btn-large">Add New Scorecard</a>
    </div>
  </div>
@stop

@section('content')



@if ($scorecard_to_edit != null)

<div class="scorecard-edit scorecard-form">

<h2 class="scorecard-form-header">Edit Existing Scorecard</h2>

{{ Form::model($scorecard_to_edit, array('route' => array('postscorecardupdate', $scorecard_to_edit->id), 'files' => true)) }}

  {{ Form::file('picture'); }}

  <fieldset>
   {{ Form::text('first_name', $scorecard_to_edit->first_name, ['placeholder' => 'First Name']); }}
   {{ Form::text('last_name', $scorecard_to_edit->last_name, ['placeholder' => 'Last Name']); }}
  </fieldset>

  <fieldset>
    {{ Form::text('party', $scorecard_to_edit->party, ['placeholder' => 'Political Party']); }}
    {{ Form::select('supports_tax', array('y' => 'Yes', 'n' => 'No', 'u' => 'Unknown'), $scorecard_to_edit->supports_tax); }}
  </fieldset>

  <fieldset>
  {{ Form::text('twitter', $scorecard_to_edit->twitter, ['placeholder' => 'Twitter']); }}
  {{ Form::text('facebook', $scorecard_to_edit->facebook, ['placeholder' => 'Facebook']); }}
  </fieldset>

  {{ Form::submit('Save Scorecard'); }}

{{ Form::close() }}

</div>
@endif

@include('slices.scorecards', ['scorecards' => $scorecards])

@stop