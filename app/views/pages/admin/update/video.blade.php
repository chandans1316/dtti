@extends('layouts.admin')

@section('back')
	<div class="row small-links">
		<div class="span12">
			<a href="/admin" class="btn btn-primary btn-large"><i class="ficon-arrow-left"></i> Go Back</a>
		</div>
	</div>
@stop

@section('content')

	<div class="row">
		<div class="span12">
			<h1 class="big-page-title">Update a Video</h1>
		</div>
	</div>


	<div class="row">
		<div class="span6">
		
			@foreach ($errors->all('<div class="alert alert-error">:message</div>') as $error)
				{{ $error }}
			@endforeach

			{{ Form::open(array('url' => 'update/'.$post->id, 'class' => 'create-form', 'id' => 'video-form', 'files'=>true)) }}
				<div class="control-group {{ $errors->first('title', 'error') }}">
					{{ Form::label('title', 'Title', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('title', Input::old('title', $post->postType->title), array('id' => 'title', 'onkeyup' => 'mirror_title();')) }}
					</div>
				</div>
				<div class="control-group">
					{{ Form::label('image','Thumbnail Image', array('class'=>'control-label'))}}
					<div class="controls">
						{{ Form::file('image') }}
					</div>
				</div>
				<div class="control-group {{ $errors->first('description', 'error') }}">
					{{ Form::label('description', 'Description', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('description', Input::old('description', $post->postType->description), array('id' => 'video_description', 'onkeyup' => 'mirror_description();')) }}
					</div>
				</div>
				<div class="control-group {{ $errors->first('youtube_url', 'error') }}">
					{{ Form::label('youtube_url', 'YouTube URL', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('youtube_url', Input::old('youtube_url', 'http://www.youtube.com/watch?v='.$post->postType->youtube_id), array('id' => 'youtube_url')) }}
					</div>
				</div>
				<div class="control-group {{ $errors->first('order', 'error') }}">
					{{ Form::label('order', 'Order #', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('order', Input::old('order', $post->order)) }}
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						  <button type="submit" class="btn">Update</button>
					</div>
				</div>
			{{ Form::close() }}
		


		</div>
		<div class="span6">
			<ul id="tiles">
				<li class="video">
				        <h5><span class="awesome-font"><i class="ficon-facetime-video"></i></span>Video</h5>
						<a rel="shadowbox" href="http://www.youtube.com/v/rz0GDSrkw-Q"><img src="https://img.youtube.com/vi/pAC5SeNH8jw/hqdefault.jpg"><div class="ficon-play"></div></a>
				        <h3><div id="example-title">{{ $post->postType->title }}</div></h3>
				        <p id="example-description">{{ $post->postType->description }}</p>
				     	
			  
				</li>
			</ul>
			<script>
				function mirror_title(){ document.getElementById('example-title').innerHTML=document.getElementById('title').value; }
				function mirror_description(){ document.getElementById('example-description').innerHTML=document.getElementById('video_description').value; }

				window.onload = function() { mirror_description(); };
				window.onload = function() { mirror_title(); };
			</script>
		</div>
	</div>

@stop