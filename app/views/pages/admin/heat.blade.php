@extends('layouts.admin')

@section('back')
	<div class="row small-links">
		<div class="span12">
			<a href="/signups" class="btn btn-primary btn-large"><i class="ficon-arrow-left"></i> Go Back</a>
		</div>
	</div>
@stop


@section('content')
<div class="row">
	<div class="span12">
		<div id="panel">
			<button onclick="toggleHeatmap()" class="btn">Toggle Heatmap</button>
			<button onclick="changeGradient()" class="btn">Change gradient</button>
			<button onclick="changeRadius()" class="btn">Change radius</button>
			<button onclick="changeOpacity()" class="btn">Change opacity</button>
		</div>
		<h3>Showing last 25,000 signups. (All signups since {{ $min_date }} )</h3>
		<div id="map"></div>
	</div>
</div>

<script>
	var map, pointarray, heatmap;



	// $.ajax({
	// 	url 		: '/signups/map',
	// 	type 		: 'POST',
	// 	dataType 	: 'json',
	// 	success 	: function(data) {

	// 		$.each(data, function(index, location) {

	// 			if (location.lat == null) {
	// 				return;
	// 			}
	// 		});

	// 	}
	// });


	

	var signups = [
		@foreach ($signups as $signup)
			@if (!is_null($signup->lat) || !is_null($signup->lng))
	  			new google.maps.LatLng({{ $signup->lat }}, {{ $signup->lng }}),
			@endif
		@endforeach
	];

	function initialize() {
	  var mapOptions = {
	    zoom: 5,
	    center: new google.maps.LatLng(39.909736, -102.084961),
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	  };

	  map = new google.maps.Map(document.getElementById('map'),
	      mapOptions);

	  var pointArray = new google.maps.MVCArray(signups);

	  heatmap = new google.maps.visualization.HeatmapLayer({
	    data: pointArray
	  });

	  heatmap.setMap(map);
	}

	function toggleHeatmap() {
	  heatmap.setMap(heatmap.getMap() ? null : map);
	}

	function changeGradient() {
	  var gradient = [
	    'rgba(0, 255, 255, 0)',
	    'rgba(0, 255, 255, 1)',
	    'rgba(0, 191, 255, 1)',
	    'rgba(0, 127, 255, 1)',
	    'rgba(0, 63, 255, 1)',
	    'rgba(0, 0, 255, 1)',
	    'rgba(0, 0, 223, 1)',
	    'rgba(0, 0, 191, 1)',
	    'rgba(0, 0, 159, 1)',
	    'rgba(0, 0, 127, 1)',
	    'rgba(63, 0, 91, 1)',
	    'rgba(127, 0, 63, 1)',
	    'rgba(191, 0, 31, 1)',
	    'rgba(255, 0, 0, 1)'
	  ]
	  heatmap.setOptions({
	    gradient: heatmap.get('gradient') ? null : gradient
	  });
	}

	function changeRadius() {
	  heatmap.setOptions({radius: heatmap.get('radius') ? null : 20});
	}

	function changeOpacity() {
	  heatmap.setOptions({opacity: heatmap.get('opacity') ? null : 0.2});
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>



@stop
