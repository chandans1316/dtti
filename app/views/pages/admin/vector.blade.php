@extends('layouts.admin')

@section('back')
	<div class="row small-links">
		<div class="span12">
			<a href="/signups" class="btn btn-primary btn-large"><i class="ficon-arrow-left"></i> Go Back</a>
		</div>
	</div>
@stop


@section('content')
<h3>Showing last 25,000 signups. (All signups since {{ $min_date }} )</h3>
<div id="map"></div>

<script>
	var density = {
		@foreach ($signups as $key => $value)
		"{{ $key }}": {{ $value }},
		@endforeach
	};

	$(function() {
		$('#map').vectorMap({
			map: 'us_aea_en',
			series: {
				regions: [{
					scale: ['#EBC7D6', '#630709'],
					attribute: 'fill',
					values: density,
					min: 0,
					max: 1586,
					normalizeFunction: 'polynomial'
				}]
			},
			onRegionLabelShow: function(event, label, code) {
				label.html(label.html()+'<br>Total Signups: '+density[code]);
			},
			backgroundColor: 'none',
			zoomOnScroll: false 
		});
	});
	
	
	</script>

	
@stop
