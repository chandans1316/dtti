@extends('layouts.admin')

@section('back')
  <div class="row small-links">
    <div class="span12">
      <a href="/admin" class="btn btn-primary btn-large"><i class="ficon-arrow-left"></i> Go Back</a>
    </div>
  </div>
@stop

@section('content')

<div class="scorecard-create scorecard-form">

<h2 class="scorecard-form-header">Create New Scorecard</h2>

{{ Form::open(array('url' => 'create/scorecard', 'files' => true)) }}
  {{ Form::file('picture'); }}

  <fieldset>
  {{ Form::text('first_name', '', ['placeholder' => 'First Name']); }}
  {{ Form::text('last_name', '', ['placeholder' => 'Last Name']); }}
  </fieldset>

<fieldset>
  {{ Form::text('party', '', ['placeholder' => 'Political Party']); }}
  {{ Form::select('supports_tax', array('y' => 'Yes', 'n' => 'No', 'u' => 'Unknown'), 'u'); }}
</fieldset>

<fieldset>
  {{ Form::text('twitter', '', ['placeholder' => 'Twitter']); }}
  {{ Form::text('facebook', '', ['placeholder' => 'Facebook']); }}
</fieldset>

  {{ Form::submit('Create New Scorecard'); }}

{{ Form::close() }}

</div>

@include('slices.scorecards', ['scorecards' => $scorecards])

@stop