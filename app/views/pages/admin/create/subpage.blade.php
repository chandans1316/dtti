@extends('layouts.admin')

@section('back')
	<div class="row small-links">
		<div class="span12">
			<a href="/admin" class="btn btn-primary btn-large"><i class="ficon-arrow-left"></i> Go Back</a>
		</div>
	</div>
@stop

@section('content')

	<div class="row">
		<div class="span12">
			<h1 class="big-page-title" align="middle">Create a Subpage</h1>
		</div>
	</div>


	<div class="row">
		<div class="span6">
			@foreach ($errors->all('<div class="alert alert-error">:message</div>') as $error)
				{{ $error }}
			@endforeach

			{{ Form::open(array('url' => 'create/subpage', 'class' => 'create-form', 'id' => 'article-form', 'files'=>true )) }}
				

				<div class="control-group {{ $errors->first('title', 'error') }}">
					{{ Form::label('title', 'Title', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('title', Input::old('title'), array('class' => 'span4')) }}
					</div>
				</div>

				<div class="control-group {{ $errors->first('source', 'error') }}">
					{{ Form::label('source', 'Source', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('source', Input::old('source'), array('class' => 'span4')) }}
					</div>
				</div>
				
				<div class="control-group {{ $errors->first('author', 'error') }}">
					{{ Form::label('author', 'Author', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('author', Input::old('author'), array('class' => 'span4')) }}
					</div>
				</div>

				<div class="control-group {{ $errors->first('date', 'error') }}">
					{{ Form::label('date', 'Date', array('class' => 'control-label')) }}
					<div class="controls input-append date" id="datepicker" data-date="{{ Input::old('date') }}" data-date-format="dd-mm-yyyy">
						{{ Form::text('date', Input::old('date'), array('class' => 'span4')) }}
						<span class="add-on"><i class="ficon-calendar"></i></span>
					</div>
				</div>

				<div class="control-group {{ $errors->first('excerpt', 'error') }}">
					{{ Form::label('excerpt', 'Excerpt', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::textarea('excerpt', Input::old('excerpt'), array('class' => 'span4')) }}
					</div>
				</div>		

				<div class="control-group {{ $errors->first('content', 'error') }}">
					{{ Form::label('content', 'Content', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::textarea('content', Input::old('content'), array('class' => 'span4')) }}
					</div>
				</div>

				<div class="control-group">
					{{ Form::label('thumb_image','Thumbnail Image', array('class'=>'control-label'))}}
					<div class="controls">
						{{ Form::file('thumb_image') }}
					</div>
				</div>

				<div class="control-group">
					{{ Form::label('banner_image','Banner Image', array('class'=>'control-label'))}}
					<div class="controls">
						{{ Form::file('banner_image') }}
					</div>
				</div>

				<div class="control-group {{ $errors->first('order', 'error') }}">
					{{ Form::label('order', 'Order #', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('order', Input::old('order')) }}
					</div>
				</div>

				<div class="control-group">
					<div class="controls">
						  <button type="submit" class="btn">Create</button>
					</div>
				</div>
			{{ Form::close() }}
		


		</div>
		<div class="span6"> 
			<ul id="tiles">
				<li class="news">
			        <h5><span class="icon-article-alt"></span>News</h5>

					<img src="/img/banner-sample.png" alt="Cato Institute Logo" />
			        
			        <h3 id="the-title-sample">Title Goes Here</h3>
			        <p><a href="#"><span id="the-source">thesource.com</span> //</a> <span id="the-excerpt-excerpt">This is the excerpt!</span></p>
			        <a href="#" class="read-more">Read&raquo;</a>
		        </li>
			</ul>
			<script language="javascript">
				$(function() {
					$("#title").keyup(function () {
						$("#the-title-sample").html($("#title").val());
					});
					$("#title").keyup();

					$("#excerpt").keyup(function () {
						$("#the-excerpt-excerpt").html($("#excerpt").val());
					});
					$("#excerpt").keyup();

					$("#source").keyup(function () {
						var val = $("#source").val();

						if (val[0] == 'h') {
							val = val.substring(7);
							if (val[0] == '/') {
								val = val.substring(1);
							}
						}

						$("#the-source").html(val);
					});
					$("#source").keyup();
				});
			</script>
		</div>
	</div>

@stop