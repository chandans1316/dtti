@extends('layouts.admin')

@section('back')
	<div class="row small-links">
		<div class="span12">
			<a href="/admin" class="btn btn-primary btn-large"><i class="ficon-arrow-left"></i> Go Back</a>
		</div>
	</div>
@stop

@section('content')

	<div class="row">
		<div class="span12">
			<h1 class="big-page-title" align="middle">Create an Article</h1>
		</div>
	</div>


	<div class="row">
		<div class="span6">

			@foreach ($errors->all('<div class="alert alert-error">:message</div>') as $error)
				{{ $error }}
			@endforeach

			{{ Form::open(array('url' => 'create/article', 'class' => 'create-form', 'id' => 'article-form', 'files'=>true )) }}
				<div class="control-group {{ $errors->first('title', 'error') }}">
					{{ Form::label('title', 'Title', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('title', Input::old('title'), array('id' => 'title', 'onkeyup' => 'mirror_title();')) }}
					</div>
				</div>
				<div class="control-group {{ $errors->first('excerpt', 'error') }}">
					{{ Form::label('excerpt', 'Excerpt', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::textarea('excerpt', Input::old('excerpt'), array('id' => 'excerpt', 'onkeyup' => 'mirror_excerpt();')) }}
					</div>
				</div>
				<div class="control-group">
					{{ Form::label('image','Image', array('class'=>'control-label'))}}
					<div class="controls">
						{{ Form::file('image') }}
					</div>
				</div>
				<div class="control-group {{ $errors->first('source', 'error') }}">
					{{ Form::label('source', 'Source', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('source', Input::old('source')) }}
					</div>
				</div>
				<div class="control-group {{ $errors->first('url', 'error') }}">
					{{ Form::label('url', 'URL', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('url', Input::old('url')) }}
					</div>
				</div>
				<div class="control-group {{ $errors->first('order', 'error') }}">
					{{ Form::label('order', 'Order #', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('order', Input::old('order')) }}
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						  <button type="submit" class="btn">Create</button>
					</div>
				</div>
			{{ Form::close() }}
		


		</div>
		<div class="span6"> 
			<ul id="tiles">
				<li class="news">
			        <h5><span class="icon-article-alt"></span>News</h5>

					<img src="/img/banner-sample.png" alt="Cato Institute Logo" />
			        
			        <h3 id="the-title-sample">Title Goes Here</h3>
			        <p><a href="#">thesource.com //</a> <span id="the-excerpt-excerpt">This is the excerpt!</span></p>
			        <a href="#" class="read-more">Read&raquo;</a>
		        </li>
			</ul>
			<script>
				function mirror_title(){ document.getElementById('the-title-sample').innerHTML=document.getElementById('title').value; }
				function mirror_excerpt(){ document.getElementById('the-excerpt-excerpt').innerHTML=document.getElementById('excerpt').value; }
				window.onload = function() { mirror_excerpt(); };
				window.onload = function() { mirror_title(); };
			</script>
		</div>
	</div>

@stop