@extends('layouts.admin')

@section('back')
  <div class="row small-links">
    <div class="span12">
      <a href="/admin" class="btn btn-primary btn-large"><i class="ficon-arrow-left"></i> Go Back</a>
    </div>
  </div>
@stop

@section('content')

<div class="FAQ-create FAQ-form">

<h2 class="FAQ-form-header">Create New FAQ</h2>

{{ Form::open(array('url' => 'create/faq')) }}

<fieldset class="faq-question">
  <h2 class="FAQ-form-header">Question*</h2>
  {{ Form::textarea('question', '', ['placeholder' => 'Question']); }}
</fieldset>

<fieldset class="faq-question">
  <h2 class="FAQ-form-header">Answer*</h2>
  {{ Form::textarea('answer', '', ['placeholder' => 'Answer']); }}
</fieldset>

  {{ Form::submit('Create New FAQ'); }}

{{ Form::close() }}

</div>

@include('slices.faq', ['faqs' => $faqs])

@stop
