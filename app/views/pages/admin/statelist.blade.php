@extends('layouts.admin')


@section('content')

	<div class="row">
		<div class="span12">
			<h1 class="big-page-title" align="middle">States</h1>
		
			<div class="dashboard-links">
				<a href="/create/state" class="btn btn-primary btn-large"><i class="ficon-group"></i> New State</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="span12">
			<h2><strong>Total Count: </strong>{{ count($states) }}</h2>
			<table id="sort-me" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Headline</th>
						<th>Subheadline</th>
						<th>URL Slug</th>
						<th>Delete</th>
						
					</tr>
				</thead>
				<tbody>
					@foreach($states as $state)
					<tr>
						<td>{{ $state->id }}</td>
						<td><a href="/updatestate/{{ $state->id }}">{{ $state->name }}</a></td>
						<td>{{ $state->headline }}</td>
						<td>{{ $state->subheadline }}</td>
						<td><a href="/{{ $state->url }}">/{{ $state->url }}</a></td>
						<td><a href="/deletestate/{{ $state->id }}">DELETE</a></td>
						
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

@stop