<?php
Route::controller('tweets', 'TweetsController');
Route::controller('signups', 'SignupsController');
Route::controller('login', 'LoginController');
Route::controller('admin', 'AdminController');
Route::controller('create', 'CreateController');

//Scorecard CRUD routes
Route::get('create/scorecard', array('as' => 'createscorecard', 'uses' => 'CreateController@getScorecard'));
Route::post('create/scorecard', array('as' => 'createscorecard', 'uses' => 'CreateController@postScorecard'));
Route::get('deletescorecard/{id}', 'DeleteController@deleteScorecard');
Route::get('updatescorecard/{id}', array('as' => 'updatescorecard', 'uses' => 'UpdateController@getScorecard'));
Route::post('updatescorecard/{id}', array('as' => 'postscorecardupdate', 'uses' => 'UpdateController@postScorecard'));
Route::post('reorder/scorecards', array('as' => 'updatescorecardorder', 'uses' => 'UpdateController@updateScorecardOrder'));

//FAQ CRUD routes
Route::post('create/faq', array('as' => 'createFAQ', 'use' => 'CreateController@postFaq'));
Route::get('create/faq', array('as' => 'createFAQ', 'use' => 'CreateController@getFaq'));
Route::post('updatefaq/{id}', array('as' => 'postupdatefaq', 'uses' => 'UpdateController@postFaq'));
Route::get('updatefaq/{id}', array('as' => 'updatefaq', 'uses' => 'UpdateController@getFaq'));
Route::post('reorder/faqs', array('as' => 'updatefaqorder', 'uses' => 'UpdateController@updateFaqOrder'));
Route::get('deletefaq/{id}', 'DeleteController@deleteFaq');

Route::get('delete/{id}', 'DeleteController@deletePost');
Route::get('deletestate/{id}', 'DeleteController@deleteState');

Route::any('update/{id}', 'UpdateController@updatePost');
Route::any('updatestate/{id}', 'UpdateController@updateState');


Route::get('news/{slug}', 'NewsController@showNews');

Route::get('privacy-policy', function()
{
	return View::make('pages.privacy')
							->with('current_page', 'privacy-policy');
});

Route::get('logout', function()
{
	Auth::logout();
	return Redirect::to('login')->with('message', 'You have been successfully logged out!');
});

Route::get('/indexold', 'HomeController@getIndexOld');
Route::post('/indexold', 'HomeController@postIndexOld');

Route::get('/', 'HomeController@getIndex');
Route::post('/', 'HomeController@postIndex');

Route::get('/social', 'SocialController@getIndex');

Route::get('/scorecard', 'ScorecardController@getIndex');

Route::get('/frequently-asked-questions', 'FaqsController@getIndex');
Route::get('/faqs', 'FaqsController@getIndex');
Route::get('/FAQ', 'FaqsController@getIndex');


Route::get('/{stateurl}', 'StatesController@showState');

Route::get('widget/template', function () {
	$buttons = array(
		array(
			'label' => 'Read More',
			'url' => 'http://donttaxtheinter.net'
		),
		array(
			'label' => 'Tweet #NoNetTax',
			'url' => 'https://twitter.com/intent/tweet?text=' . urlencode("Don't let Congress pass an Internet sales tax that could cost your family $360! Tell them: No! http://bit.ly/1ywmEzh #NoNetTax")
		)
	);
	$button = $buttons[array_rand($buttons)];
	$backgroundNo = mt_rand(1, 2);

	$widgetStat = new WidgetStat;
	$widgetStat->user_id = (Input::has('userId') && Input::get('userId') != 'null') ? Input::get('userId') : Session::getId();
	$widgetStat->session_id = (Input::has('sessionId') && Input::get('sessionId') != 'null') ? Input::get('sessionId') : Session::getId();
	$widgetStat->url = Input::get('url');
	$widgetStat->user_agent = Request::server('HTTP_USER_AGENT');
	$widgetStat->ip_address = Request::server('REMOTE_ADDR');
	$widgetStat->background_no = $backgroundNo;
	$widgetStat->button_label = $button['label'];
	$widgetStat->button_url = $button['url'];
	$widgetStat->widget_size = (Input::has('size') && Input::get('size') != 'null') ? strtoupper(Input::get('size')) : 'SMALL';
	$widgetStat->clicks = 0;
	$widgetStat->save();

	$response = Response::json(array(
		'background' => 'background' . $backgroundNo,
		'button' => $button,
		'session_id' => $widgetStat->session_id,
		'user_id' => $widgetStat->user_id,
	));
	$response->headers->set('Access-Control-Allow-Origin', '*');
	return $response;
});

Route::get('widget/click', function () {
	$widgetStat = WidgetStat::where('session_id', Input::get('sessionId'))->first();
	$widgetStat->clicks = $widgetStat->clicks + 1;
	$widgetStat->save();

	$response = Response::json(array('success' => true));
	$response->headers->set('Access-Control-Allow-Origin', '*');
	return $response;
});

// Set 404 page
App::missing(function ($exception) {
	if (Str::contains(Request::path(), '/')) {
		return Response::view('pages.404', array(), 404);
	} else {
		return App::make('HomeController')->getIndex();
	}
});