<?php

use Illuminate\Database\Migrations\Migration;

class CreateFaqTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	  Schema::create('faq', function($table)
		{
				$table->increments('id');
				$table->integer('order');
		    $table->string('question');
		    $table->string('answer');
		    $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
    		$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('faq', function($table)
		{
			$table->drop('faq');
		});
	}

}