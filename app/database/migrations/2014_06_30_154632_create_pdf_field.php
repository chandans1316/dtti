<?php

use Illuminate\Database\Migrations\Migration;

class CreatePdfField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
			
		Schema::table('states', function($table)
		{
		    $table->string('pdf_file_name')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('states', function($table)
		{
		    $table->dropColumn('pdf_file_name');
		});
	}

}