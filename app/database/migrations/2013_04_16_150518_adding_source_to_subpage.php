<?php

use Illuminate\Database\Migrations\Migration;

class AddingSourceToSubpage extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('post_type_subpage', function($table)
		{
			$table->string('source');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('post_type_subpage', function($table)
		{
			$table->dropColumn('source');
		});
	}
}