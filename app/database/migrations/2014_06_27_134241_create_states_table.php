<?php

use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('states', function($table)
		{
		    $table->increments('id');
		    $table->string('name');
		   	$table->string('headline');
		   	$table->text('subheadline');
		   	$table->string('url');
		   	$table->text('body')->nullable();
			$table->string('image_file_name')->nullable();
			$table->boolean('dropdown')->default(1)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('states');
	}

}