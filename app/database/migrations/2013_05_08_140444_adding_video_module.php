<?php

use Illuminate\Database\Migrations\Migration;

class AddingVideoModule extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('post_type_video', function($table)
		{
		    $table->increments('id');
		   	$table->integer('post_id');
		   	$table->string('title');
			$table->string('image_file_name')->nullable();
			$table->text('description')->nullable();
			$table->string('youtube_id');
		});

		Schema::table('posts', function($table)
		{
		    $table->dropColumn('post_type');
		});

		Schema::table('posts', function($table)
		{
		    $table->enum('post_type', array('QUOTE', 'ARTICLE', 'SUBPAGE', 'VIDEO'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('post_type_video');

		Schema::table('posts', function($table)
		{
		    $table->dropColumn('post_type');
		});

		Schema::table('posts', function($table)
		{
			$table->enum('post_type', array('QUOTE', 'ARTICLE', 'SUBPAGE'));
		});
	}

}