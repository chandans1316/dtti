<?php

use Illuminate\Database\Migrations\Migration;

class CreateWidgetStatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('widget_stats', function($table)
		{
		    $table->increments('id');
		    $table->string('user_id')->index();
		    $table->string('session_id')->index();
		    $table->string('url');
		    $table->string('user_agent');
		    $table->string('ip_address');
		    $table->integer('background_no')->unsigned();
		    $table->string('button_label');
		    $table->string('button_url');
		    $table->enum('widget_size', array('SMALL', 'LARGE'))->default('SMALL');
		    $table->integer('clicks')->unsigned()->default(0);
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('widget_stats');
	}

}