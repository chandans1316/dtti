<?php

use Illuminate\Database\Migrations\Migration;

class UpdateArticleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('post_type_article', function($table)
		{
			$table->string('source')->after('url');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('post_type_article', function($table)
		{
			$table->dropColumn('source');
		});
	}

}